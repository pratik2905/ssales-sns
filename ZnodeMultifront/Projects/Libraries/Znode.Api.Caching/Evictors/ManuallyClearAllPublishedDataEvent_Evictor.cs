﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Api.Caching.Core;
using Znode.Libraries.Caching;
using Znode.Libraries.Caching.Events;
using Znode.Libraries.ECommerce.Utilities;

namespace Znode.Api.Caching.Evictors
{
    internal class ManuallyClearAllPublishedDataEvent_Evictor : BaseApiEvictor<ManuallyClearAllPublishedDataEvent>
    {
        protected override void Setup(ManuallyClearAllPublishedDataEvent cacheEvent)
        {

        }

        protected override void EvictNonDictionaryCacheData(ManuallyClearAllPublishedDataEvent cacheEvent)
        {

        }

        protected override List<string> EvictSpecificDictionaryCacheKeys(ManuallyClearAllPublishedDataEvent cacheEvent)
        {
            return new List<string>();
        }

        protected override bool IsDictionaryItemStale(ManuallyClearAllPublishedDataEvent cacheEvent, string key)
        {
            return true;
        }

        protected override void Teardown(ManuallyClearAllPublishedDataEvent cacheEvent)
        {

        }
    }
}
