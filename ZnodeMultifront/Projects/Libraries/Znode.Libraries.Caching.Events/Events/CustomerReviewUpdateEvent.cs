﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Znode.Libraries.Caching.Events
{
    public class CustomerReviewUpdateEvent : BaseCacheEvent
    {
        public int PortalId { get; set; }
    }
}
