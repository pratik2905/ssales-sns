﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Znode.Libraries.Caching.Events
{
    public class ManuallyRefreshWebStoreCacheEvent : BaseCacheEvent
    {
        public int[] DomainIds { get; set; }
    }
}
